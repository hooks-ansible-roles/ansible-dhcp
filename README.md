dhcp
=========

Creates DHCP server for a single subnet. Has a dict for multiple subnets, but is not implemented yet.

Requirements
------------

none

Role Variables
--------------

Defaults are just some sane defaults.

vars/main.yml has a dict for dhcp server package names on some different distros.

Needs a dictionary of hosts named records for reservations:

    records:
      server1: {type: A, last: 1, mac: }

Dependencies
------------

none

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: dhcp-servers
      roles:
         - { role: username.rolename, x: 42 }

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
